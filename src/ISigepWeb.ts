export interface ICep {
  bairro: string;
  cep: string;
  cidade: string;
  complemento2: string;
  end: string;
  uf: string;
}

export interface IChancela {
  chancela: string;
  dataAtualizacao: string;
  descricao: string;
  id: string;
}

export interface IServicosSigep {
  categoriaServico: string;
  chancela: IChancela;
  descricao: string;
  exigeDimensoes: string;
  exigeValorCobrar: string;
  imitm: string;
  pagamentoEntrega: string;
  remessaAgrupada: string;
  restricao: string;
  servico: string;
  ssiCoCodigoPostal: string;
}

export interface IVigencia {
  dataFinal: string;
  dataInicial: string;
  datajFim: string;
  datajIni: string;
  id: string;
}

export interface IServicos {
  codigo: string;
  dataAtualizacao: string;
  datajAtualizacao: string;
  descricao: string;
  horajAtualizacao: string;
  id: string;
  servicoSigep: IServicosSigep;
  servicosAdicionais: string;
  tipo1Codigo: string;
  tipo2Codigo: string;
  vigencia: IVigencia;
}

export interface ICartoesPostagem {
  codigoAdministrativo: string;
  dataAtualizacao: string;
  dataVigenciaFim: string;
  dataVigenciaInicio: string;
  datajAtualizacao: string;
  datajVigenciaFim: string;
  datajVigenciaInicio: string;
  horajAtualizacao: string;
  numero: string;
  servicos: IServicos[];
  statusCartaoPostagem: string;
  statusCodigo: string;
  unidadeGenerica: string;
}

export interface IContratoPK {
  diretoria: string;
  numero: string;
}

export interface IContratos {
  cartoesPostagem: ICartoesPostagem[];
  codigoCliente: string;
  codigoDiretoria: string;
  contratoPK: IContratoPK;
  dataAtualizacao: string;
  dataAtualizacaoDDMMYYYY: string;
  dataVigenciaFim: string;
  dataVigenciaFimDDMMYYYY: string;
  dataVigenciaInicio: string;
  dataVigenciaInicioDDMMYYYY: string;
  datajAtualizacao: string;
  datajVigenciaFim: string;
  datajVigenciaInicio: string;
  descricaoDiretoriaRegional: string;
  horajAtualizacao: string;
  statusCodigo: string;
}

export interface ICliente {
  cnpj: string;
  contratos: IContratos[];
  dataAtualizacao: string;
  datajAtualizacao: string;
  descricaoStatusCliente: string;
  horajAtualizacao: string;
  id: string;
  inscricaoEstadual: string;
  nome: string;
  statusCodigo: string;
}

export interface IPlp {
  id_plp: string;
  valor_global: string;
  mcu_unidade_postagem: string;
  nome_unidade_postagem: string;
  cartao_postagem: number;
}

export interface IRemetente {
  numero_contrato: number;
  numero_diretoria: string;
  codigo_administrativo: number;
  nome_remetente: string;
  logradouro_remetente: number;
  numero_remetente: string;
  complemento_remetente: string;
  bairro_remetente: string;
  cep_remetente: string;
  cidade_remetente: string;
  uf_remetente: string;
  telefone_remetente: string;
  fax_remetente: string;
  email_remetente: string;
  celular_remetente: string;
  cpf_cnpj_remetente: string;
  ciencia_conteudo_proibido: string;
}

export interface IDestinatario {
  nome_destinatario: string;
  telefone_destinatario: string;
  celular_destinatario: string;
  email_destinatario: string;
  logradouro_destinatario: string;
  complemento_destinatario: string;
  numero_end_destinatario: string;
  restricao_anac: string;
  cpf_cnpj_destinatario: string;
}

export interface INacional {
  bairro_destinatario: string;
  cidade_destinatario: string;
  uf_destinatario: string;
  cep_destinatario: string;
  codigo_usuario_postal: string;
  centro_custo_cliente: string;
  numero_nota_fiscal: string;
  serie_nota_fiscal: string;
  valor_nota_fiscal: string;
  natureza_nota_fiscal: string;
  descricao_objeto: string;
  valor_a_cobrar: string;
}

export interface ICodigoServicoAdicional {
  codigo_servico_adicional: ICodigoServicoAdicional[];
}

export interface IServicoAdicional {
  valor_declarado: string;
  codigo_servico_adicional: ICodigoServicoAdicional;
}

export interface IDimensaoObjeto {
  tipo_objeto: string;
  dimensao_altura: string;
  dimensao_largura: string;
  dimensao_comprimento: string;
  dimensao_diametro: string;
}

export interface IObjetoPostal {
  numero_etiqueta: string;
  codigo_objeto_cliente: string;
  codigo_servico_postagem: string;
  cubagem: string;
  peso: string;
  rt1: string;
  rt2: string;
  restricao_anac: string;
  destinatario: IDestinatario;
  nacional: INacional;
  servico_adicional: IServicoAdicional;
  dimensao_objeto: IDimensaoObjeto;
  data_captacao: string;
  data_postagem_sara: string;
  status_processamento: number;
  numero_comprovante_postagem: string;
  valor_cobrado: string;
}

export interface ICorreiosLog {
  tipo_arquivo: string;
  versao_arquivo: string;
  plp: IPlp;
  remetente: IRemetente;
  forma_pagamento: string;
  objeto_postal: IObjetoPostal[];
}

export interface IPreCorreiosLog {
  correioslog: ICorreiosLog;
}
